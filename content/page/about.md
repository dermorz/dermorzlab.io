---
title: About this blog
comments: false
---
My name is Moritz Bracht and this is my personal blog. This is where I write
about learnings - mostly from technical nature or somehow tech industry
related.

There might be some off-topic posts or personal rants here and there.

## Disclaimer

*The opinions expressed in this blog are my own and not necessarily those of my
employer.*

## Who am I

I am a dad of 2 and got lucky to be married to the biggest inspiration of my
life. Besides enjoying quality family time I love to go fishing, cooking
delicious food or tinkering hardware like my self-built split mechanical
keyboard.

### Current role: Senior Software Engineer @ [Kubermatic][kubermatic.com]

Since August 2021 I am working for [Kubermatic][kubermatic.com]. I will update
this section after being assigned to a project/product.

[kubermatic.com]: https://www.kumermatic.com

### Background

#### 2020-2021: Senior Software Engineer @ [radio.de][radio.net]

Since 2020 I was working for [radio.de][radio.net]. Our small backend team was
providing the APIs powering all apps and webapps. These APIs were almost all
implemented as AWS Lambdas or micro-services on AWS ECS and written in go. For
podcast data there were also backend-services, doing distributed data
processing.

Our team was also handling operations. Since we were fully committed to the AWS
stack, we were managing everything with CloudFormation or Terraform aws-modules.

[radio.net]: https://corporate.radio.net/

#### 2016-2020: Senior Software Engineer @ [finleap Connect][finleap Connect]

On my team I was mostly working on a financial services API which mostly was an
assembly of python applications and go microservices. All our services were
running on a self-hosted bare-metal kubernetes cluster. I was also supporting
the on-call team doing incident management and improving alerting on the k8s
cluster and our services. Before we migrated to k8s, our infrastructure was a
bunch of linux machines managed via openstack, terraform and ansible.

[finleap Connect]: https://connect.finleap.com/

#### 2013-2016: Senior Python Developer @ [ABOUT YOU][aboutyou]

I was building and maintaining the connector to multiple OTTO backends. ABOUT
YOU started out as an OTTO universe start-up piggy-backing on their product
portfolio and logistics before integrating more product sources.

[aboutyou]: https://www.aboutyou.de/

#### 2009-2013: Software Developer at [Datenlotsen][datenlotsen]

I had to use the "language" [Magic][magic]. The product was actually not bad,
but I never want to write one line of code in that language anymore. Whereas
"line of code" and "language" are both totally missing the concept of Magic.

[datenlotsen]: http://www.datenlotsen.de/
[magic]: https://www.magicsoftware.com/app-development-platform/xpa/
