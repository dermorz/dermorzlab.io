---
title: SQL scripts with gomplate and docker
categories:
  - tools
date: 2021-06-25
tags:
  - 100daystooffload
  - shell
draft: true

---

Recently I came across one of these tasks, where some data needed to be written to a table, depending on some other data.
On the one hand it seemed to trivial and one-time-ish to write code with parametrized queries.
Also deploying and running a code solution would have had quite some overhead for this little task.
On the other hand it was too complex to copy/paste and modify the roughly 100 queries in total without messing something up.
Reviewing these queries is also a nightmare because everything looks so similar and the relevant details are quite hidden in where clauses and join conditions.
