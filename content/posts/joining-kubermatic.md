---
title: I am joining Kubermatic
categories:
  - dear diary
date: 2021-08-03
tags:
  - 100daystooffload

---
After only 13 months at radio.de I will be joining [Kubermatic][kubermatic.com] in August 2021.
Long story short without going into details there was too much of a (engineering-)culture gap between radio.de and me.

I am now looking forward to become an active contributor in the “Cloud Native” world, working on open-source projects around Kubernetes.
In my previous role at finleap connect I had only “consumed” Kubernetes as an engineer running software on it and during on-call duties, including monitoring and basic maintenance.
In the application process at Kubermatic I had to write my first controller and I really enjoyed learning a lot about K8s internals.

It’s not yet decided which project I will be joining, but it might be one of the SAP-teams working on [Kyma][kyma-project.io].
Kubermatic has teamed up with SAP to contribute to this really interesting open-source project.

Either way I am happy to bring in my Go knowledge and will be constantly improve my Kube Fu.

[kubermatic.com]: https://www.kubermatic.com
[kyma-project.io]: https://www.kyma-project.io
