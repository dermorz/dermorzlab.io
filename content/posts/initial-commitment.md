---
title: initial commitment
date: 2020-01-22
categories:
  - dear diary
---
I wanted to start this blog quite some time ago, I think at least 5 years ago.
What kept me from actually doing it might have been a combination of
procrastination and imposter syndrome.

To tackle the procrastination I am defining goals for 2020. To keep the chances
of achieving these goals realistic I try to keep the list quite small and will
divide each goal into sub-goals and will schedule a monthly check on how the
progress on these goals is.

## 2020 Career Goals

* Pass my CKA exam
* Get more comfortable writing go
* Reiterate my future career plan
* Start and keep blogging

### CKA

The CKA exam will happen after I completed [Linux Foundation's Kubernetes
Fundamentals (LFS258)][1], which is a self-paced course, but is limited to one
year. I enrolled at on 2019-09-16 and my employer [finleap Connect][2] is
paying for it, so I have a 2-dimensional motivation here: A hard deadline
(2020-09-16) and not wanting to waste the money of my employer.

### golang

My experience with go is still kind of limited. We have a few go projects at
work and I also have a small hobby project using serverless go at [zeit.co][3],
but I still very often don't know the idiomatic way of doing things. Sure I can
solve problems, but coming from the python universe I want to want to be able
to choose _the one right way to do things_. That's why I am currently doing the
[go track on exercism][4] when I find the time. I feel like the mentor's input
gives me what i need to know to be able to write more idiomatic go code. Even
if the first few exercises seem "boring" to solve, but actually not using time
and energy in finding _a solution_ but the most idiomatic one feels like a good
investment.

### future career

Regarding my future career plan I feel like I need to do some soul searching.
My second son was just born and for half a year I am on parental leave. This
puts me in the comfortable situation to reflect my current job situation, the
current state of the software industry and if there is room for improvement.
Improvement would not necessarily mean to quit my job and start something else
but after evaluating my own market value I feel way more confident to push my
career into the direction I would like it to be.

But what do I actually want?  For one part definitely _enough_ time with my
growing family. Having 2 kids was a conscious choice so not being at home for
50-60 hours every week (40hrs plus lunch breaks and commute) feels kind of
wrong. So even staying on my current job with a bigger share of home-office
would be a big improvement.

Even if I exclude the whole work-life-balance topic, there are other questions
to think about: Do I want to be _promoted_ to (middle-)management or are there
other career paths where I can keep doing what I love but still move forward?
Do I have the courage to leave my comfort zone and start being self-employed or
pack my family to move to another country? The said soul search has not even
begun. I really need to find a way to structure my thoughts on everything.

### blog

All of the goals above feel kind of natural to come together in the fourth one.
Even only the 3 first career goals for 2020 seem to provide enough fuel to be
able to keep this blog at least semi-active.

I will also try to use this blog to keep track on the progress of the other
goals. Keeping track of the progress should help me identify if I get stuck
somewhere.

## imposter syndrome

The other thing I want this blog to help me with is fighting imposter syndrome.
Every time wanted to start this blog I felt that the information I wanted to
share was too trivial or low-level. Who would read a blog entry where someone
just learned about some topic or does not fully grasp everything around that
topic? But after listening to the [go time podcast episode #30][5] where the
hosts discuss the imposter syndrome I realized what I actually knew before: It
is ok to not know everything or master everything from the beginning.  And
maybe there are other people out there exploring new topics can actually profit
from my insights I share while learning something.

I guess imposter syndrome is also coupled to anxiety of being wrong
or perfectionism. So to overcome it in my case where it held me back
from writing my blog, it really helps to admit: It's is ok to be
wrong sometimes and it's especially ok when we are in the process of
learning.

_Note to myself: Write a rant about the school system where we systematically
got punished for mistakes instead of embracing the opportunity to learn from
these mistakes._

[1]: https://training.linuxfoundation.org/training/kubernetes-fundamentals/
[2]: https://connect.finleap.com/
[3]: https://zeit.co/
[4]: https://exercism.io/tracks/go
[5]: https://changelog.com/gotime/30
