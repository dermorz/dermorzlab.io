---
title: "100 Days to Offload"
date: 2021-06-12T21:33:01+02:00
tags:
  - 100DaysToOffload
categories:
  - meta
---

While searching for some solution I came around some blog post tagged with
`#100DaysToOffload` and got curious what that meant. So I found this site
[100daystooffload.com](https://100daystooffload.com/) and now I wonder, if I actually have enough topics to get out 100 blog entries over a year.

I actually have quite a backlog of topics, but something kept me from just
writing about them. Probably because some of them are just too big to fit into
a tweet. But I really don't like these huge twitter threads, so writing a small
blog post feels way more convenient.

Let's see in a year how this worked out and keep the spirit of this challenge:

**Just. Write.**
