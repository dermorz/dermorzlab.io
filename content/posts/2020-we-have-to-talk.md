---
title: Looking back at 2020
date: 2021-01-12
categories:
  - dear diary
---
2020 has been a challenging year in
many ways and I don’t want to
reiterate the whole Corona thing,
but it surely had some impact on my
initial plan for that year:

> ## 2020 Career Goals
>
> * Pass my CKA exam
> * Get more comfortable writing go
> * Reiterate my future career plan
> * Start and keep blogging

Because I left my former employer
I stopped pursuing the CKA. On the
parental leave I was busy parenting
and after that my focus at work
switched to the AWS Cloud. But maybe
one day I will turn back to the
K8s-world.

Since starting at radio.de
I switched from “doing some Go” to
being a full-time Go Developer and
I love it. The community, the
opinionated nature of this small
language and the tooling amaze me.
There is no way I will be going back
to Python anytime soon. I am still
learning here and there, but I feel
really comfortable writing and
reviewing Go code.

Regarding my future career I did
switch jobs in mid 2020, but I also
found out for myself that I do not
see myself being “promoted” to
management anytime soon. That career
path seems to be weirdly common in
Germany and I only know of very few
instances where roles like principal
or staff engineer even exist. Google
for example has these roles in
theory, but a friend at Google
Germany told me, that there are no
engineers above “Senior Engineer”
(Level 5) at Google Munich. Level
6 would be “Staff Engineer” or
“Intermediate Manager” but the
former seems to be virtually
non-existent.

So a mid- to long-term goal would be
to join a company where a future
career path as individual
contributor is defined and possible.

As of starting my blog and keeping
updates coming I want to use Corona
as an excuse, but the other half of
the truth would be a lack of
motivation and energy to do anything
intellectually demanding or creative
after 8 hours of work plus 4 more
hours of handling the kids to give
my wife a break.

Maybe 2021 will be different, but
right now it is Corona-main-season.
Thankfully residing at my in-laws
gives us some slack.
